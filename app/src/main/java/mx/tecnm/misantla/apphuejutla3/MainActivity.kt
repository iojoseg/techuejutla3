package mx.tecnm.misantla.apphuejutla3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import mx.tecnm.misantla.apphuejutla3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btnCalcular.setOnClickListener {
            val edad = binding.edtEdad.text.toString()

            if(!edad.isEmpty()){
                val edadEntera = edad.toInt()
                val result = edadEntera * 7
                binding.tvResultado.text = "La edad de su perro es $result años"
            }else{
                Toast.makeText(this, "Debe ingresar una edad",
                Toast.LENGTH_LONG).show()
            }
        }
    }
}